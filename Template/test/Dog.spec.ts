import {should} from 'chai';
import {Dog} from "../src/Dog";

should();


describe("Test dog", () => {

    const dog = new Dog();

    it("should bark", () => {
        dog.woof().should.equal("woof");
    });
});